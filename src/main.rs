use nannou::prelude::*;

fn main() {
    nannou::sketch(view).run()
}

#[derive(Copy, Clone)]
enum Orientation { Horizontal, Vertical }

#[derive(Copy, Clone)]
struct H {
    position: nannou::geom::point::Point2,
    orientation: Orientation,
    size: f32,
}

impl H {
    fn draw_H(&self, draw: &nannou::draw::Draw, window: nannou::geom::Rect, growth: f32) {

        let mut H_vec = match self.orientation {
            Orientation::Horizontal => {
                nannou::geom::point::Point2::from((self.size * growth, 0.0))
            },
            Orientation::Vertical => {
                nannou::geom::point::Point2::from((0.0, self.size * growth))
            },
        }; 

        draw.line()
            .weight(1.0)
            .caps_round()
            .color(WHITE)
            .points(self.position - H_vec, self.position + H_vec);
    }

    fn new(pos: nannou::geom::point::Point2, ori: Orientation, size: f32) -> H {
        H {
            position: pos,
            orientation: ori,
            size: size,
        }
    }

    fn from_other(other: &H, child_index: u8, growth: f32) -> H {
        let child_choice : bool = child_index & 0b01 != 0;
        let growth_size = other.size * growth;

        let child_rel_pos = match (other.orientation, child_choice) {
            (Orientation::Horizontal, false) => (-growth_size, 0.0),
            (Orientation::Horizontal, true) => (growth_size, 0.0),
            (Orientation::Vertical, false) => (0.0, -growth_size),
            (Orientation::Vertical, true) => (0.0, growth_size),
        };

        let child_pos = other.position + nannou::geom::Vector2::from(child_rel_pos);

        let child_orientation = match other.orientation {
            Orientation::Horizontal => Orientation::Vertical,
            Orientation::Vertical =>  Orientation::Horizontal,
        };

        H::new(child_pos, child_orientation, growth_size)
    }

    fn spawn_children(&self, growth: f32) -> (H, H) {
        (
            H::from_other(self, 0, growth),
            H::from_other(self, 1, growth),
        )
    }

    fn grow_and_draw(&self, mut max_generations: u64, draw: &nannou::draw::Draw, window: nannou::geom::Rect, time: f32, full_growth_time: f32) {

        let mut current_generation = 0;

        self.grow_and_draw_for_generations(current_generation, max_generations, &draw, window, time, full_growth_time);
    }

    fn grow_and_draw_for_generations(&self, mut current_generation: u64, max_generations: u64, draw: &nannou::draw::Draw, window: nannou::geom::Rect, time: f32, full_growth_time: f32) {

        if  current_generation < max_generations
        {
            let max_growth_factor = 1.0 / f32::sqrt(2.0) ;
            let growth = if time >= full_growth_time { max_growth_factor } else { max_growth_factor * time / full_growth_time };
            

            self.draw_H(&draw, window, growth);

            let generation_progress = (current_generation as f32) / (max_generations as f32);
            let time_progress = if time >= full_growth_time { 1.0 } else { time / full_growth_time };

            if time_progress >= generation_progress {
                
                current_generation += 1;

                let babies = self.spawn_children(growth);
                babies.0.grow_and_draw_for_generations(current_generation, max_generations, &draw, window, time, full_growth_time);
                babies.1.grow_and_draw_for_generations(current_generation, max_generations, &draw, window, time, full_growth_time);

            }
        }
        else
        {
            return;
        }
    }
}

fn view(app: &App, frame: Frame) {
    
    let win = app.window_rect();
    let generation : u64 = 12;
    let max_time = 15.0;
    let big_daddy = H::new(win.xy(), Orientation::Horizontal, win.w() / 3.0);

    let t = app.time;

    // Begin drawing
    let draw = app.draw();

    // Clear the background 
    draw.background().color(BLACK);

    big_daddy.grow_and_draw(generation, &draw, win, t, max_time);

    // Write the result of our drawing to the window's frame.
    draw.to_frame(app, &frame).unwrap();

    // // Capture the frame!
    // if t <= max_time {
    //     let file_path = captured_frame_path(app, &frame);
    //     app.main_window().capture_frame(file_path);
    // }
}

fn captured_frame_path(app: &App, frame: &Frame) -> std::path::PathBuf {
    // Create a path that we want to save this frame to.
    app.project_path()
        .expect("failed to locate `project_path`")
        // Capture all frames to a directory called `/<path_to_nannou>/nannou/simple_capture`.
        .join(app.exe_name().unwrap())
        // Name each file after the number of the frame.
        .join(format!("{:03}", frame.nth()))
        // The extension will be PNG. We also support tiff, bmp, gif, jpeg, webp and some others.
        .with_extension("png")
}